﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TurnerLiz.Startup))]
namespace TurnerLiz
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
