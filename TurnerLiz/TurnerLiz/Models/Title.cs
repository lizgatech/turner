﻿using System;
using System.Data.Entity;

namespace TurnerLiz.Models
{
	public class Title
	{
		public int TitleId
		{
			get; set;
		}
		public string TitleName
		{
			get; set;
		}
		public string TitleNameSortable
		{
			get; set;
		}
		public int TitleTypeId
		{
			get; set;
		}
		public int ReleaseYear
		{
			get; set;
		}
		public DateTime ProcessedDateTimeUTC
		{
			get; set;
		}		
	}
	public class TitleDBContext : DbContext
	{
		public DbSet<Title> Titles
		{
			get; set;
		}
	}
}